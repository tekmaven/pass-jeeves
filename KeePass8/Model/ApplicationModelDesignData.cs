﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using KeePassLib.Serialization;
using Windows.Storage;
using Windows.Storage.Streams;

namespace KeePass8.Model
{
    public class ApplicationModelDesignData : IApplicationModel
    {
        public Collection<IOConnectionInfo> RecentFiles { get; set; }

        public ApplicationModelDesignData()
        {
            RecentFiles = new Collection<IOConnectionInfo>();
            RecentFiles.Add(new IOConnectionInfo());
            RecentFiles.Add(new IOConnectionInfo());
            RecentFiles.Add(new IOConnectionInfo());
        }


        public Task Save()
        {
            return null;
        }

        public Task Load()
        {
            return null;
        }

        public void AddRecent(IOConnectionInfo ioc)
        {
        }
    }
}
