﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePassLib;
using KeePassLib.Keys;
using KeePassLib.Serialization;

namespace KeePass8.Model
{
    class DatabaseDesignData : IDatabaseModel
    {
        public event EventHandler<DatabaseChangedEventArgs> DatabaseChanged;
       
        public PwDatabase GetDatabase()
        {
            PwDatabase pwDatabase = new PwDatabase();

            PwGroup pwGroup = new PwGroup(false, false, "Root", PwIcon.Apple);
            pwDatabase.RootGroup = pwGroup;

            for (int i = 0; i < 10; i++)
            {
                PwGroup group = new PwGroup(false, false, "Test_"+i, PwIcon.Apple);

                for (int j = 0; j < 10; j++)
                {
                    PwEntry entry = new PwEntry(false, false);
                    entry.Strings.Set(PwDefs.TitleField, new KeePassLib.Security.ProtectedString(false, "Entry"));
                    entry.Strings.Set(PwDefs.UserNameField, new KeePassLib.Security.ProtectedString(false, "Joe Blow"));
                    entry.Strings.Set(PwDefs.UrlField, new KeePassLib.Security.ProtectedString(false, @"http://here.com"));
                    entry.Strings.Set(PwDefs.NotesField, new KeePassLib.Security.ProtectedString(false, "Remember to bring back home a milk carton and a bread"));
                    entry.IconId = PwIcon.CDRom;
                    group.Entries.Add(entry);
                }


                pwDatabase.RootGroup.AddGroup(group, true);
            }
            return pwDatabase;
        }


        public Task<PwDatabase> OpenDatabase(IOConnectionInfo ioConnection, CompositeKey cmpKey, bool bOpenLocal)
        {
            return null;
        }


        public void SetDatabase(PwDatabase db)
        {
        }


        public Exception ErrorCode { get; set; }
   
    }
}
