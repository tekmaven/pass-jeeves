﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePassLib;
using KeePassLib.Keys;
using KeePassLib.Serialization;

namespace KeePass8.Model
{
    public class DatabaseChangedEventArgs : System.EventArgs
    {
        /* Empty for the time being*/
    }

    public interface IDatabaseModel
    {
        event EventHandler<DatabaseChangedEventArgs> DatabaseChanged;
        PwDatabase GetDatabase();
        void SetDatabase(PwDatabase db);

        /// <summary>
        /// Open a KeePass database
        /// </summary>
        /// <param name="ioConnection"></param>
        /// <param name="cmpKey"></param>
        /// <param name="bOpenLocal"></param>
        /// <returns>
        /// A reference to the database in case opening succeeds.
        /// In case of failure, returns null. The error code can be fetched through the ErrorCode property.
        /// </returns>
        Task<PwDatabase> OpenDatabase(IOConnectionInfo ioConnection, CompositeKey cmpKey, bool bOpenLocal);

        Exception ErrorCode { get; }
    }
}
