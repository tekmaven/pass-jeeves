﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using KeePassLib;
using KeePassLib.Collections;
using KeePassLib.Keys;
using KeePassLib.Serialization;
using Windows.Storage;
using Windows.UI.Xaml.Media;

namespace KeePass8.Model
{

    public class DatabaseModel : IDatabaseModel
    {
        private static DatabaseModel m_KeePassModel;
        private PwDatabase m_pwDatabase;

        public event EventHandler<DatabaseChangedEventArgs> DatabaseChanged;
        public Exception ErrorCode { get; private set; }

        public DatabaseModel()
        {
            ErrorCode = null;
            m_pwDatabase = new PwDatabase(); /* An empty object */
        }

        public PwDatabase GetDatabase()
        {
            if (m_pwDatabase != null)
                return m_pwDatabase;
            else
                 throw new NullReferenceException();
        }

        public void SetDatabase(PwDatabase db)
        {
            m_pwDatabase = db;
            if (DatabaseChanged != null)
                DatabaseChanged(this, new DatabaseChangedEventArgs());
        }

        /// <summary>
        /// Open a KeePass database
        /// </summary>
        /// <param name="ioConnection"></param>
        /// <param name="cmpKey"></param>
        /// <param name="bOpenLocal"></param>
        /// <returns>
        /// A reference to the database in case opening succeeds.
        /// In case of failure, returns null. The error code can be fetched separately.
        /// </returns>
        public async Task<PwDatabase> OpenDatabase(IOConnectionInfo ioConnection, CompositeKey cmpKey, bool bOpenLocal)
        {
            ErrorCode = null;
            Debug.Assert(ioConnection != null);
            Debug.Assert(cmpKey != null);

#if TODO
			if(!ioConnection.CanProbablyAccess())
			{
				MessageService.ShowWarning(ioConnection.GetDisplayName(), KPRes.FileNotFoundError);
				return;
			}

			if(OpenDatabaseRestoreIfOpened(ioConnection)) return;
#endif

            PwDatabase pwOpenedDb = null;
            pwOpenedDb = await OpenDatabaseInternal(ioConnection, cmpKey);

            m_pwDatabase = pwOpenedDb;
            if (DatabaseChanged != null)
                DatabaseChanged(this, new DatabaseChangedEventArgs());

            return pwOpenedDb;

        }



        private async Task<PwDatabase> OpenDatabaseInternal(IOConnectionInfo ioc, CompositeKey cmpKey)
        {

            PwDatabase pwDb = new PwDatabase();

            try
            {
                await pwDb.Open(ioc, cmpKey, null /*swLogger */);
            }
            catch (Exception ex)
            {
                ErrorCode = ex;
                pwDb = null;
            }

            return pwDb;
        }

    }
}