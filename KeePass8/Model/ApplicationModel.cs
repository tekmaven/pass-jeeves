﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using KeePassLib.Serialization;
using Windows.Storage;
using Windows.Storage.Streams;
using System.IO;

namespace KeePass8.Model
{
    public class ApplicationModel : IApplicationModel
    {

        public Collection<IOConnectionInfo> RecentFiles { get; set; }

        private string _SerializedApplicationStateFileName = "SessionState.xml";

        public ApplicationModel()
        {
            RecentFiles = new Collection<IOConnectionInfo>();
        }


        public async Task Save()
        {
            // Get the output stream for the SessionState file.
            StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(_SerializedApplicationStateFileName,
                     CreationCollisionOption.ReplaceExisting);

            IRandomAccessStream raStream = await file.OpenAsync(FileAccessMode.ReadWrite);
            using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
            {
                // Serialize the Session State.
                DataContractSerializer serializer = new DataContractSerializer(typeof(Collection<IOConnectionInfo>));
                serializer.WriteObject(outStream.AsStreamForWrite(), RecentFiles);
                await outStream.FlushAsync();
            }
        }

        public async Task Load()
        {
            // Get the input stream for the SessionState file.

            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(_SerializedApplicationStateFileName);
                if (file == null) return;
                IInputStream inStream = await file.OpenSequentialReadAsync();

                // Deserialize the Session State.
                DataContractSerializer serializer = new DataContractSerializer(typeof(Collection<IOConnectionInfo>));
                Collection<IOConnectionInfo> recentFiles = (Collection<IOConnectionInfo>)serializer.ReadObject(inStream.AsStreamForRead());

                foreach (IOConnectionInfo fileInfo in recentFiles)
                {
                   RecentFiles.Add(fileInfo);
                }
            }

            catch (Exception)
            {
                // Restoring state is best-effort.  If it fails, the app will just come up with a new session.
            }
        }

        public void AddRecent(IOConnectionInfo ioc)
        {
            RecentFiles.Remove(ioc);
            RecentFiles.Add(ioc);
        }

    }
}
