﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace KeePass8.WinRT_UI
{
    public sealed partial class KeePassFileInfo : UserControl
    {
        public KeePassFileInfo()
        {
            this.InitializeComponent();
        }
    }

    public class FileSizePresenter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            ulong fileSize = (ulong)value;
            float fileSizeInKB = (float)fileSize / 1024;

            return fileSizeInKB.ToString("0.0") + " kB"; ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return (ulong)0;
        }
    }
}
