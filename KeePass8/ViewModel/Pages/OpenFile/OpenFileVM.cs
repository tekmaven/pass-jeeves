﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using KeePass8.Model;
using KeePass8.WinRT_UI;
using KeePassLib.Serialization;
using Microsoft.Practices.ServiceLocation;
using Windows.ApplicationModel.Resources;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace KeePass8.ViewModel.Pages
{
    public class OpenFileVM : ViewModelBase
    {
        private NotificationMessageActionWithParameter<OpenFileMessage> _openFileMessage;
        private FileTypeItem _selectedFileType = null;
        private FileTypeItem _defaultFileType = null;
        private string _selectedFileTypeState;

        public const string SelectedFileTypeStatePropertyName = "SelectedFileTypeState";
        public const string SelectedFileTypePropertyName = "SelectedFileType";

        public OpenFileVM()
        {
            Messenger.Default.Register<NotificationMessageActionWithParameter<OpenFileMessage>>(this, MessageReceived);
            Messenger.Default.Register<NotificationMessage<FilePickedMessage>>(this, FilePickedCallback);
            FileTypes = new Collection<FileTypeItem>();
            _defaultFileType = new FileTypeItem
            {
                Title = LocalizeString("RecentFile"),
                Description = LocalizeString("RecentFileDesc"),
                Image = "ms-appx:///Assets/MetroStyle/New.png",
                FileTypeState = "Recent"
            };
            SelectedFileType = _defaultFileType;
            FileTypes.Add(_defaultFileType);
            FileTypes.Add(new FileTypeItem
            {
                Title = LocalizeString("LocalFile"),
                Description = LocalizeString("LocalFileDesc"),
                Image = "ms-appx:///Assets/MetroStyle/Monitor.png",
                OnClickCommand = new RelayCommand(() => PickLocalFile()),
                FileTypeState = "Local"
            });
            FileTypes.Add(new FileTypeItem
            {
                Title = LocalizeString("URLFile"),
                Description = LocalizeString("URLFileDesc"),
                Image = "ms-appx:///Assets/MetroStyle/Cloud_Download.png",
                OnClickCommand = new RelayCommand(() => NotYetSupported()),
                FileTypeState = "URL"
            });
            FileTypes.Add(new FileTypeItem
            {
                Title = LocalizeString("SkyDriveFile"),
                Description = LocalizeString("SkyDriveFileDesc"),
                Image = "ms-appx:///Assets/MetroStyle/SkyDrive.jpeg",   
                OnClickCommand = new RelayCommand(() => NotYetSupported()),
                FileTypeState = "SkyDrive"
            });
        }


        public Collection<FileTypeItem> FileTypes { get; set; }

        /// <summary>
        /// Sets and gets the SelectedFileType property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        /// 
        public FileTypeItem SelectedFileType
        {
            get
            {
                return _selectedFileType;
            }

            set
            {
                if (_selectedFileType == value)
                {
                    return;
                }
                
                RaisePropertyChanging(SelectedFileTypePropertyName);
                _selectedFileType = value;
                RaisePropertyChanged(SelectedFileTypePropertyName);

                SelectedFileTypeState = value.FileTypeState;
                if (_selectedFileType.OnClickCommand != null)
                    _selectedFileType.OnClickCommand.Execute(null);
            }
        }

        /* The state of the page, defines the user control to show */

        public string SelectedFileTypeState
        {
            get
            {
                return _selectedFileTypeState;
            }

            private set
            {
                if (_selectedFileTypeState == value)
                {
                    return;
                }
                
                RaisePropertyChanging(SelectedFileTypeStatePropertyName);
                _selectedFileTypeState = value;
                RaisePropertyChanged(SelectedFileTypeStatePropertyName);
            }
        }


        /// <summary>
        /// This function is called when a component broadcasts a message of type OpenFileMessage.
        /// Upon reception of that message, displays a FileOpen dialog.
        /// </summary>

        private async void MessageReceived(NotificationMessageActionWithParameter<OpenFileMessage> message)
        {
            _openFileMessage = message;
            SelectedFileType = _defaultFileType;

            // Display the UI
            Frame navigation = Window.Current.Content as Frame;
            if (navigation != null)
                navigation.Navigate(typeof(OpenFilePage));
        }


        private async Task PickLocalFile()
        {

            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".kdbx");
            StorageFile file = await openPicker.PickSingleFileAsync();
            if (file == null)
            {
                SelectedFileType = _defaultFileType;
                return;
            }

            IOConnectionInfo ioc = IOConnectionInfo.FromPath(file.Path);
            Messenger.Default.Send((new NotificationMessage<FilePickedMessage>(
                new FilePickedMessage { IOConnectionInfo = ioc },
                "Local file picked")));

        }

        private async Task NotYetSupported()
        {
            MessageDialog dialog = new MessageDialog("Not yet implemented");
            await dialog.ShowAsync();
        }


        // This message is received when user has selected a file in a userControl
        private void FilePickedCallback(NotificationMessage<FilePickedMessage> message)
        {

            IOConnectionInfo ioc = message.Content.IOConnectionInfo;
            Messenger.Default.Send((new NotificationMessageActionWithParameter<UserAuthentificationDialogMessage>(
                           new UserAuthentificationDialogMessage { IOConnectionInfo = ioc },
                           UserAuthentificationDialogCallback)));
        }

        // This message is received when the authorization dialog is dismissed.
        private void UserAuthentificationDialogCallback(UserAuthentificationDialogMessage message)
        {
            if (message.Success)
                SuccessOpeningFile(message);
        }


        private void SuccessOpeningFile(UserAuthentificationDialogMessage message)
        {
            // Set the database to the one just opened.
            ServiceLocator.Current.GetInstance<IDatabaseModel>().SetDatabase(message.Model.GetDatabase());
            // Add the newly opened database in the recent list.
            ServiceLocator.Current.GetInstance<IApplicationModel>().AddRecent(message.Model.GetDatabase().IOConnectionInfo);

            // Dismiss this dialog.
            Frame navigation = Window.Current.Content as Frame;
            if (navigation != null)
            {
                navigation.GoBack();
            }
            // Indicates to the calling view that file opening is completed. 
            _openFileMessage.Execute(new OpenFileMessage { Success = message.Success });
        }

        private string LocalizeString(string source)
        {
#if DEBUG
            if (ViewModelBase.IsInDesignModeStatic)
                return source;
#endif
            ResourceLoader rl = new ResourceLoader();
            return rl.GetString(source);
        }

        public string LocalizedPageTitle { get { return LocalizeString("OpenFilePageTitle"); } }

    }

    public class FileTypeItem
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public RelayCommand OnClickCommand { get; set; }
        public string FileTypeState { get; set; }
    }
}
