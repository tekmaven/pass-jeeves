﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using KeePassLib;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace KeePass8.ViewModel.Pages
{
    public class EmptyPageNoFilesOpenedVM : ViewModelBase
    {
        public RelayCommand OpenFileCommand
        {
            get;
            private set;
        }

        public RelayCommand OpenURLCommand
        {
            get;
            private set;
        }

        public RelayCommand CreateNewFileCommand
        {
            get;
            private set;
        }


        public EmptyPageNoFilesOpenedVM()
        {
            OpenFileCommand = new RelayCommand(() => OpenLocalFile());
            CreateNewFileCommand = new RelayCommand(() => CreateFile());
        }

        private void OpenLocalFile()
        {
            Messenger.Default.Send((new NotificationMessageActionWithParameter<OpenFileMessage>(
                                        new OpenFileMessage { isLocalFile = true },
                                        OpenFileCallback)));
        }

        private async Task CreateFile()
        {
            MessageDialog dialog = new MessageDialog("Not yet implemented");
            await dialog.ShowAsync();
        }

        private void OpenFileCallback(OpenFileMessage message)
        {
            if (message.Success)
            {
                Frame navigation = Window.Current.Content as Frame;
                if (navigation != null)
                {
                    navigation.Navigate(typeof(GroupedPasswordItemGridPage));
                }
            }
        }

    }
}
