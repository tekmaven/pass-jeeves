/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:KeePass8"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using KeePass8.Model;
using KeePass8.ViewModel.Pages;
using KeePass8.ViewModel.Pages.OpenFile;
using Microsoft.Practices.ServiceLocation;

namespace KeePass8.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if ( (ViewModelBase.IsInDesignModeStatic) && SimpleIoc.Default.IsRegistered<IDatabaseModel>())
                /*  Blend, for some reason, fancies initializing the ViewModelLocator more than once. */
                return;


            // Register Models
            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IDatabaseModel, DatabaseDesignData>();
                SimpleIoc.Default.Register<IApplicationModel, ApplicationModelDesignData>();
            }
            else
            {
                SimpleIoc.Default.Register<IDatabaseModel, DatabaseModel>();
                SimpleIoc.Default.Register<IApplicationModel, ApplicationModel>();
            }


            // Register ViewModels

            SimpleIoc.Default.Register<GroupedPasswordItemVM>();
            SimpleIoc.Default.Register<EmptyPageNoFilesOpenedVM>();
            SimpleIoc.Default.Register<OpenFileRecentVM>();

            // Dialogs
            SimpleIoc.Default.Register<Clipboard>(true);
            SimpleIoc.Default.Register<OpenFileVM>(true);
            SimpleIoc.Default.Register<UserAuthentificationPageVM>(true);
        }

		/////////////////////////////////////////////
        // Singletons VMs
        public static IApplicationModel ApplicationModel
        {
            get { return ServiceLocator.Current.GetInstance<IApplicationModel>(); }
        }

        public static UserAuthentificationPageVM UserAuthentificationPageVM
        {
            get { return ServiceLocator.Current.GetInstance<UserAuthentificationPageVM>(); }
        }

        public static OpenFileVM OpenFileVM
        {
            get { return ServiceLocator.Current.GetInstance<OpenFileVM>(); }
        }

		/////////////////////////////////////////////
        // Unique VMs
        public GroupedPasswordItemVM GroupedPasswordItemViewVM
        {
            get { return ServiceLocator.Current.GetInstance<GroupedPasswordItemVM>(); }
        }

        public static EmptyPageNoFilesOpenedVM EmptyPageNoFilesOpenedVM
        {
            get { return ServiceLocator.Current.GetInstance<EmptyPageNoFilesOpenedVM>(); }
        }

        public static OpenFileRecentVM OpenFileRecentVM
        {
            get
            {
                OpenFileRecentVM vm = ServiceLocator.Current.GetInstance<OpenFileRecentVM>("unique");
                SimpleIoc.Default.Unregister(vm); // We do not want this instance to be kept in the cache.
                return vm;
            }
        }




#if DEBUG
        public PasswordItemVM PasswordItemVM
        {
            get { return this.GroupedPasswordItemViewVM.PasswordItemGroups[0].Items[0]; }
        }
#endif


        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}